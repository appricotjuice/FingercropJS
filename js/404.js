'use strict';

// Disable browser "Back" button

var disableBackBtn = function disableBackBtn() {
    window.history.forward();
};

// go back to site(by url)
var goToSite = function goToSite(url) {
    window.location.replace(url);
};
document.getElementById('backToSite').addEventListener('click', function () {
    return goToSite('./index.html');
});

document.addEventListener('DOMContentLoaded', function () {
    return disableBackBtn();
});