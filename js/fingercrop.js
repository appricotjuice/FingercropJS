"use strict";

var treshold = 0.004,
    testsCount = 3,
    testLoopCount = 10,
    testDelay = 500;

// IE detect
var isIE = function isIE() {
    var userAgent = navigator.userAgent;
    return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1;
};
//Firefox/Chrome performance test
var performanceTest = function performanceTest(testsCount, testLoopCount) {

    var min = 999,
        max = 0,
        avg = void 0,
        sum = 0,
        performanceList = [],
        performanceBefore = 0,
        performanceAfter = 0,
        loopPerformance = 0;

    for (var i = 0; i < testsCount; i++) {
        performanceBefore = performance.now();
        for (var j = 0; j < testLoopCount; j++) {
            console.log(1);
            console.clear();
        }
        performanceAfter = performance.now();
        loopPerformance = (performanceAfter - performanceBefore) / 1000;
        performanceList.push(loopPerformance);
        if (loopPerformance < min) min = loopPerformance;
        if (loopPerformance > max) max = loopPerformance;
        sum += loopPerformance;
    }

    avg = sum / testsCount;

    // document.getElementById('performance').innerHTML = `min: ${min} <br>max: ${max} <br>avg: ${avg}`;
    return avg;
};

var checkDevTools = function checkDevTools(testsCount, testLoopCount) {
    return performanceTest(testsCount, testLoopCount) > treshold;
};
// USAGE
// document.getElementById('result').innerHTML = (performanceTest(testsCount, testLoopCount) > treshold) ? 'DevTools are opened' : 'DevTools are closed';

setInterval(function () {
    if (checkDevTools(testsCount, testLoopCount)) window.location.replace('./404.html');
}, testDelay);

//prevent context
document.addEventListener('contextmenu', function (event) {
    return event.preventDefault();
});
//Detect if window is active
var blur = function blur() {
    document.body.classList.add('blured');
};
var focus = function focus() {
    document.body.classList.remove('blured');
};
window.addEventListener('blur', blur);
window.addEventListener('focus', focus);

//disable select IE4+
document.onselectstart=new Function ("return false");
//
// window.addEventListener('mouseover', ()=>{window.focus()});
// window.addEventListener('mouseout', ()=>{window.blur()});